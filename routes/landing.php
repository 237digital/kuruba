<?php

/*
  |--------------------------------------------------------------------------
  | Landing page Routes
  |--------------------------------------------------------------------------
  --
    |These routes manage the landing pages routes accessible to all users
    |
 */
  
    // Landing pages
    Route::get('/', 'LandingController@index')->name('home');

    Route::post('/language', 'LanguageController@changeLanguage')->name('language.change');

   

    //Sitemap
    Route::get('/sitemap.xml', function() {
        return base_path('sitemap.xml');
    });

    //Blog Section
    Route::get('/blog', 'BlogController@all_blog')->name('blog');
    Route::get('/blog/{slug}', 'BlogController@blog_details')->name('blog.details');


    Auth::routes(['verify' => true]);

    // Auth 
    Route::get('/social-login/redirect/{provider}', 'Auth\LoginController@redirectToProvider')->name('social.login');
    Route::get('/social-login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->name('social.callback');
    Route::get('/users/login', 'HomeController@login')->name('user.login');
    Route::get('/users/registration', 'HomeController@registration')->name('user.registration');
   
   Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
   Route::get('/email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
   Route::get('/verification-confirmation/{code}', 'Auth\VerificationController@verification_confirmation')->name('email.verification.confirmation');
   Route::get('/email_change/callback', 'HomeController@email_change_callback')->name('email_change.callback');
   Route::post('/password/reset/email/submit', 'HomeController@reset_password_with_code')->name('password.update');

   
   Route::get('/getCountries','CountryController@getCountries');
   Route::get('/getStates/{id}','StateController@getStates');
   Route::get('/getCities/{id}','CityController@getCities');
  //Newsletter
   Route::resource('subscribers', 'SubscriberController');
   
