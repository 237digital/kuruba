<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Shop;
use App\Models\Product;

class LandingController extends Controller
{
    public function index(){
        $users = User::get();
        $shops = Shop::get();
        $products = Product::get();
        return view('frontend.landing_page.index', compact('users', 'shops', 'products'));
    }
}
