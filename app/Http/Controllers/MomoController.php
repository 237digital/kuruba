<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Bmatovu\MtnMomo\Products\Collection;
use Bmatovu\MtnMomo\Exceptions\CollectionRequestException;
use App\Models\CombinedOrder;
use Session;

class MomoController extends Controller
{
    public function getPhoneNumber(){

    }

    public function pay(Request $request){
        if($request->session()->has('payment_type')) {
            if($request->session()->get('payment_type') == 'cart_payment') {
                $combined_order = CombinedOrder::findOrFail(Session::get('combined_order_id'));
                dd($combined_order);
                $amount = round($combined_order->grand_total);
            }
        }

        try {

            $collection = new Collection();
            
            //The payment informations
            $bill = ['transactionId', '00237678523163', $amount];
           
            //Pay with momo
            $momoTransactionId = $collection->requestToPay('transactionId', '00237678523163', $amount);

            dd($momoTransactionId);

            //check payment status
            $collection->getTransactionStatus($momoTransactionId);

        } catch(CollectionRequestException $e) {
            do {
                printf("\n\r%s:%d %s (%d) [%s]\n\r", 
                    $e->getFile(), $e->getLine(), $e->getMessage(), $e->getCode(), get_class($e));
            } while($e = $e->getPrevious());
        }
    }
}
