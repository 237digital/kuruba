<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaymentApis\OrangeMoney;
use Illuminate\Support\Str;

class OmController extends Controller
{
    public function pay(Request $request){
        //dd($request);
        //Getting datas values
        $order = Str::random('5');

        //Proceeding with payment
        $om = new OrangeMoney(500, $order);

        $orangePayment = $om->getPaymentUrl(url('return_url_here')); // url here is return url

        return redirect($orangePayment->payment_url);
    }
}
