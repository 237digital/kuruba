<?php

namespace App\Exports;

use App\Models\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrderExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Order::all();
    }

    public function headings(): array
    {
        return [
            'code',
            translate('Num. of Products'),
            translate('Customer'),
            translate('Amount'),
            translate('Delivery Status'),
            translate('Payment Status'),
            translate('Date'),
        ];
    }

    /**
    * @var Order $order
    */
    public function map($order): array
    {
        return [
            $order->code,
            count($order->orderDetails),
            $order->user->name,
            single_price($order->grand_total),
            $order->delivery_status,
            $order->payment_status,
            $order->created_at,
        ];
    }
}
