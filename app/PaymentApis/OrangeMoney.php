<?php

namespace App\PaymentApis;

class OrangeMoney {
    private $authorization_header = ""; 
    private $merchant_key = "";  
    private $amount = "";
    private $order_id = "";

    public function __construct($amount, $order_id){
        $this->amount = $amount; 
        $this->order_id = $order_id;
    }

    //Get access token from the basic authorisation given by Orange

    public function getAccessToken(){
        $this->authorization_header = env('OM_AUTH_HEADER');
        $ch = curl_init('https://api.orange.com/oauth/v3/token');

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: '.$this->authorization_header
        ));

        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        return json_decode(curl_exec($ch))->access_token;
    }

    //Web payment function to redirect user to Orange Payment Page

    public function getPaymentUrl($returnUrl){
        $this->merchant_key = env('OM_MERCHANT_KEY');
        $data = array(
            "merchant_key" => $this->merchant_key,
            "currency" =>"XAF",
            "order_id" => $this->order_id,
            "amount" => $this->amount, 
            "return_url" => $returnUrl,
            "cancel_url" => url('/marketplace'),
            "notif_url" => url('/notification'),
            "lang" => "fr",
        );

        //for Cameroon
        $ch = curl_init('https://api.orange.com/orange-money-webpay/cm/v1/webpay');

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer '.$this->getAccessToken(),
            'Accept: application/json',
            'Content_type: application/json',
        ));

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if (curl_errno($ch)) {
            return curl_error($ch);
        }

        return json_decode(curl_exec($ch));
    }
}
