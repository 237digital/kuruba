    @extends('backend.layouts.app')

    @section('content')

    <div class="row">
        {{--MTN Mobile Money--}}
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0 h6 ">{{translate('MTN Mobile Money Collections Credentials')}}</h5>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{ route('payment_method.update') }}" method="POST">
                        <input type="hidden" name="payment_method" value="mtn_momo">
                        @csrf
                        <div class="form-group row">
                            <input type="hidden" name="types[]" value="MOMO_COLLECTION_SUBSCRIPTION_KEY">
                            <div class="col-md-4">
                                <label class="col-from-label">{{translate('MTN MOMO Collection Subscription key')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="MOMO_COLLECTION_SUBSCRIPTION_KEY" value="{{  env('MOMO_COLLECTION_SUBSCRIPTION_KEY') }}" placeholder="{{ translate('MOMO Collection Subscription key') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <input type="hidden" name="types[]" value="MOMO_COLLECTION_SECRET">
                            <div class="col-md-4">
                                <label class="col-from-label">{{translate('MOMO Collection Secret')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="MOMO_COLLECTION_SECRET" value="{{  env('MOMO_COLLECTION_SECRET') }}" placeholder="{{ translate('MTN MOMO Collection Secret') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="col-from-label">{{translate('MTN MOMO Sandbox Mode')}}</label>
                            </div>
                            <div class="col-md-8">
                                <label class="aiz-switch aiz-switch-success mb-0">
                                    <input value="1" name="momo_sandbox" type="checkbox" @if (env('MOMO_ENVIRONMENT') == 'sandbox')
                                        checked
                                    @endif>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group mb-0 text-right">
                            <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{--Orange Money--}}
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0 h6 ">{{translate('Orange Money Credentials')}}</h5>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{ route('payment_method.update') }}" method="POST">
                        @csrf
                        <input type="hidden" name="payment_method" value="om">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="col-from-label">{{translate('Authorization Header')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="OM_AUTH_HEADER" value="{{  env('OM_AUTH_HEADER') }}" placeholder="{{ translate('OM Authorization Header') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <input type="hidden" name="types[]" value="OM_MERCHANT_KEY">
                            <div class="col-md-4">
                                <label class="col-from-label">{{translate('Merchant Key')}}</label>
                            </div>
                            <div class="col-md-8">
                            <input type="text" class="form-control" name="OM_MERCHANT_KEY" value="{{  env('OM_MERCHANT_KEY') }}" placeholder="{{ translate('OM Merchant KEY') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="col-from-label">{{translate('Return url')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="OM_RETURN_URL" value="{{  env('OM_RETURN_URL') }}" placeholder="{{ translate('OM Return URL') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="col-from-label">{{translate('Notification url')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="OM_NOTIF_URL" value="{{  env('OM_NOTIF_URL') }}" placeholder="{{ translate('OM Notification URL') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="col-from-label">{{translate('Cancel url')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="OM_CANCEL_URL" value="{{  env('OM_CANCEL_URL') }}" placeholder="{{ translate('OM Cancellation URL') }}" required>
                            </div>
                        </div>
                        <div class="form-group mb-0 text-right">
                            <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{--PayPal--}}
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0 h6 ">{{translate('Paypal Credential')}}</h5>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{ route('payment_method.update') }}" method="POST">
                        <input type="hidden" name="payment_method" value="paypal">
                        @csrf
                        <div class="form-group row">
                            <input type="hidden" name="types[]" value="PAYPAL_CLIENT_ID">
                            <div class="col-md-4">
                                <label class="col-from-label">{{translate('Paypal Client Id')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="PAYPAL_CLIENT_ID" value="{{  env('PAYPAL_CLIENT_ID') }}" placeholder="{{ translate('Paypal Client ID') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <input type="hidden" name="types[]" value="PAYPAL_CLIENT_SECRET">
                            <div class="col-md-4">
                                <label class="col-from-label">{{translate('Paypal Client Secret')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="PAYPAL_CLIENT_SECRET" value="{{  env('PAYPAL_CLIENT_SECRET') }}" placeholder="{{ translate('Paypal Client Secret') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="col-from-label">{{translate('Paypal Sandbox Mode')}}</label>
                            </div>
                            <div class="col-md-8">
                                <label class="aiz-switch aiz-switch-success mb-0">
                                    <input value="1" name="paypal_sandbox" type="checkbox" @if (get_setting('paypal_sandbox') == 1)
                                        checked
                                    @endif>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group mb-0 text-right">
                            <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{--Stripe--}}
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0 h6 ">{{translate('Stripe Credential')}}</h5>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{ route('payment_method.update') }}" method="POST">
                        @csrf
                        <input type="hidden" name="payment_method" value="stripe">
                        <div class="form-group row">
                            <input type="hidden" name="types[]" value="STRIPE_KEY">
                            <div class="col-md-4">
                                <label class="col-from-label">{{translate('Stripe Key')}}</label>
                            </div>
                            <div class="col-md-8">
                            <input type="text" class="form-control" name="STRIPE_KEY" value="{{  env('STRIPE_KEY') }}" placeholder="{{ translate('STRIPE KEY') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <input type="hidden" name="types[]" value="STRIPE_SECRET">
                            <div class="col-md-4">
                                <label class="col-from-label">{{translate('Stripe Secret')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="STRIPE_SECRET" value="{{  env('STRIPE_SECRET') }}" placeholder="{{ translate('STRIPE SECRET') }}" required>
                            </div>
                        </div>
                        <div class="form-group mb-0 text-right">
                            <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    @endsection
