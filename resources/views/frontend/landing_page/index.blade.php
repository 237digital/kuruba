@extends('frontend.layouts.landing')

@section('content')

    <!-- HERO-6
        ============================================= -->
        <section id="hero-6" class="hero-section division">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <!-- HERO TEXT -->
                    <div class="col-lg-6">
                        <div class="hero-6-txt">

                            <!-- Title -->
                            <h2 class="h2-md">{{translate('Your B2B marketplace 100% made in africa')}}</h2>

                            <!-- Text -->
                            <p class="p-lg">Découvrez une façon encore plus optimale de distribuer vos produits partout dans le monde
                            </p>

                            <!-- HERO QUICK FORM -->
                            <form name="quickform" class="quick-form shadow-form">
                                @guest
                                <!-- Form Inputs -->
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a href="{{ url('users/registration') }}" class="btn btn-black tra-orange-hover white-color">{{ ucfirst(translate('I am a seller')) }}</a>
                                    </span>
                                    <span class="input-group-btn form-btn">
                                        <a href="{{ url('users/registration') }}" class="btn btn-orange tra-orange-hover last-link white-color">{{ ucfirst(translate('I am a customer')) }}</a>
                                    </span>
                                </div>
                                @else
                                <!-- Form Inputs -->
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a href="{{ url('marketplace') }}" class="btn btn-black tra-orange-hover white-color">{{ ucfirst(translate('Go to marketplace')) }}</a>
                                    </span>
                                    {{-- <span class="input-group-btn form-btn">
                                        <a href="{{ url('users/registration') }}" class="btn btn-orange tra-orange-hover last-link white-color">{{ ucfirst(translate('I am a customer')) }}</a>
                                    </span> --}}
                                </div>
                                @endguest

                                <!-- Form Message -->
                                <div class="quick-form-msg"><span class="loading"></span></div>

                            </form>

                        </div>
                    </div>	<!-- END HERO TEXT -->


                    <!-- HERO IMAGE -->
                    <div class="col-lg-6">
                        <div class="hero-6-img text-center">
                            <img class="img-fluid" src="{{static_asset('landing_page/assets/images/dashboard-04.png')}}" alt="hero-image">
                        </div>
                    </div>


                </div>    <!-- End row -->
            </div>	   <!-- End container -->


            <!-- WAVE SHAPE BOTTOM -->
            <div class="wave-shape-bottom">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 215"><path fill="#ffffff" fill-opacity="1" d="M0,128L120,149.3C240,171,480,213,720,208C960,203,1200,149,1320,122.7L1440,96L1440,320L1320,320C1200,320,960,320,720,320C480,320,240,320,120,320L0,320Z"></path></svg>
            </div>


        </section>
    <!-- END HERO-6 -->

    <!-- FEATURES-8
        ============================================= -->
        <section id="how-it-works" class="wide-60 features-section division">
            <div class="container">
                <!-- SECTION TITLE -->
               {{--  <div class="row justify-content-center">
                    <div class="col-lg-10 col-xl-8">
                        <div class="section-title title-01 mb-70">

                            <!-- Title -->
                            <h2 class="h2-md">Get Ready to Be Surprised</h2>

                            <!-- Text -->
                            <p class="p-xl">Aliquam a augue suscipit, luctus neque purus ipsum neque dolor primis a libero
                                tempus, blandit and cursus varius and magnis sapien
                            </p>

                        </div>
                    </div>
                </div> --}}


                <!-- FEATURES-8 WRAPPER -->
                <div class="fbox-8-wrapper text-center">
                    <div class="row row-cols-1 row-cols-md-3">


                        <!-- FEATURE BOX #1 -->
                        <div class="col">
                            <div class="fbox-8 mb-40 wow fadeInUp">

                                <!-- Image -->
                                <div class="fbox-img bg-whitesmoke-gradient">
                                    <img class="img-fluid" src="{{static_asset("landing_page/assets/images/img-21.png")}}" alt="feature-icon" />
                                </div>

                                <!-- Title -->
                                <h5 class="h5-md">Créez votre boutique</h5>

                                <!-- Text -->
                                <p class="p-lg">Créez votre compte et configurez votre boutique en quelques clics
                                </p>

                            </div>
                        </div>

                        <!-- FEATURE BOX #2 -->
                        <div class="col">
                            <div class="fbox-8 mb-40 wow fadeInUp">

                                <!-- Image -->
                                <div class="fbox-img bg-whitesmoke-gradient">
                                    <img class="img-fluid" src="{{static_asset("landing_page/assets/images/img-25.png")}}" alt="feature-icon" />
                                </div>

                                <!-- Title -->
                                <h5 class="h5-md">Trouvez des revendeurs</h5>

                                <!-- Text -->
                                <p class="p-lg">Distribuez vos produits auprès de revendeurs fiables et dynamiques
                                </p>

                            </div>
                        </div>

                        <!-- FEATURE BOX #3 -->
                        <div class="col">
                            <div class="fbox-8 mb-40 wow fadeInUp">

                                <!-- Image -->
                                <div class="fbox-img bg-whitesmoke-gradient">
                                    <img class="img-fluid" src="{{static_asset("landing_page/assets/images/img-14.png")}}" alt="feature-icon" />
                                </div>

                                <!-- Title -->
                                <h5 class="h5-md">Contrôlez vos performances</h5>

                                <!-- Text -->
                                <p class="p-lg">Optimisez vos ventes avec une analyse approfondie de vos résultats
                                </p>

                            </div>
                        </div>


                    </div>  <!-- End row -->
                </div>	<!-- END FEATURES-8 WRAPPER -->


            </div>	   <!-- End container -->
        </section>	
    <!-- END FEATURES-8 -->


        <!-- CONTENT-2
        ============================================= -->
        <section id="about" class="content-2 bg-snow wide-60 content-section division">
            <div class="container">
                <div class="row d-flex align-items-center">


                    <!-- IMAGE BLOCK -->
                    <div class="col-md-5 col-lg-6">
                        <div class="rel img-block left-column wow fadeInRight">
                            <img class="img-fluid" src="{{static_asset("landing_page/assets/images/img-10.png")}}" alt="video-preview">
                        </div>
                    </div>


                    <!-- TEXT BLOCK -->
                    <div class="col-md-7 col-lg-6">
                        <div class="txt-block right-column wow fadeInLeft">

                            <!-- Section ID -->
                            <span class="section-id txt-upcase">Bienvenue sur Kuruba</span>

                            <!-- Title -->
                            <h2 class="h2-xs">La vitrine des marques !</h2>
                            <p class="p-lg">
                                Kuruba est une plateforme digitale qui met en relation les marques, les producteurs et les distributeurs (locaux et internationaux) avec les détaillants tout en mettant particulièrement en avant le label “Made in Africa”, et en proposant des solutions logistiques optimales à ses utilisateurs.
                            </p>

                        </div>
                    </div>	<!-- END TEXT BLOCK -->


                </div>	   <!-- End row -->
            </div>	   <!-- End container -->
        </section>	<!-- END CONTENT-2 -->


			<!-- DIVIDER LINE -->
			<hr class="divider">


			<!-- STATISTIC-2
			============================================= -->
			<section id="statistic-2" class="wide-100 statistic-section division">
				<div class="container">
					<div class="row d-flex align-items-center">


						<!-- TEXT BLOCK -->
						<div class="col-lg-6">
							<div class="txt-block right-column wow fadeInLeft">
								<h3 class="h3-xs">Rejoignez les {{ number_format($users->count()) }}  utilisateurs déjà inscrits sur KURUBA</h3>
							</div>
						</div>


						<!-- STATISTIC BLOCK #1 -->
						<div class="col-sm-5 col-md-4 col-lg-3 offset-sm-1 offset-md-2 offset-lg-1">
							<div class="statistic-block wow fadeInUp">

								<!-- Text -->
								<h2 class="h2-title-xs statistic-number"><span class="count-element">{{ $products->count() }} </span></h2>
								<p class="p-lg txt-400">Produits disponibles

							</div>
						</div>


						<!-- STATISTIC BLOCK #2 -->
						<div class="col-sm-5 col-md-4 col-lg-2">
							<div class="statistic-block wow fadeInUp">

								<!-- Text -->
								<h2 class="h2-title-xs statistic-number">
									<span class="count-element">{{ $shops->count() }} </span>
								</h2>

								<p class="p-lg txt-400">Boutiques</p>
							</div>
						</div>


					</div>    <!-- End row -->
				</div>	   <!-- End container -->
			</section>	<!-- END STATISTIC-2 -->




			<!-- DIVIDER LINE -->
			<hr class="divider">




		{{-- 	<!-- CONTENT-6
			============================================= -->
			<section id="content-6" class="content-6 wide-60 content-section division">
			 	<div class="container">
			 		<div class="row d-flex align-items-center">


			 			<!-- TEXT BLOCK -->
			 			<div class="col-md-6 col-lg-5">
			 				<div class="txt-block left-column wow fadeInRight">

					 			<!-- TEXT BOX -->
								<div class="txt-box mb-30">

									<!-- Title -->
									<h5 class="h5-lg">Creative and Practical</h5>

									<!-- Text -->
									<p class="p-lg">Quaerat sodales sapien euismod blandit undo vitae ipsum primis and cubilia
									   a laoreet augue and luctus magna dolor egestas luctus sapien vitae nemo egestas volute
									   and turpis
									</p>

								</div>

								<!-- TEXT BOX -->
								<div class="txt-box">

									<!-- Title -->
									<h5 class="h5-lg">Multiplatform. Always Synced</h5>

									<!-- List -->
									<ul class="simple-list">

										<li class="list-item">
											<p class="p-lg">Fringilla risus, luctus mauris an auctor purus euismod iaculis luctus
											   magna purus pretium ligula and quaerat luctus magna
											</p>
										</li>

										<li class="list-item">
											<p class="p-lg">Nemo ipsam egestas volute turpis dolores undo ultrice aliquam quaerat
											   sodales
											</p>
										</li>

									</ul>

								</div>	<!-- END TEXT BOX -->

					 		</div>
					 	</div>


			 			<!-- IMAGE BLOCK -->
						<div class="col-md-6 col-lg-7">
							<div class="img-block right-column wow fadeInLeft">
								<img class="img-fluid" src="{{static_asset('landing_page/assets/images/img-19.png')}}" alt="content-image">
							</div>
						</div>


					</div>     <!-- End row -->
			 	</div>      <!-- End container -->
			</section> --}}	 <!-- END CONTENT-6 -->




			<!-- CONTENT-9
			============================================= -->
			{{-- <section id="content-9" class="content-9 bg-04 pt-100 content-section division">
				<div class="container white-color">


					<!-- SECTION TITLE -->
					<div class="row justify-content-center">
						<div class="col-md-10 col-lg-8">
							<div class="section-title title-02 mb-60">

								<!-- Section ID -->
						 		<span class="section-id txt-upcase">Handling With Ease</span>

						 		<!-- Title -->
								<h2 class="h2-xs">Discover powerful features to boost your productivity</h2>

							</div>
						</div>
					</div>


					<!-- IMAGE BLOCK -->
					<div class="row">
						<div class="col">
							<div class="content-9-img video-preview wow fadeInUp">

								<!-- Play Icon -->
								<a class="video-popup1" href="https://www.youtube.com/embed/SZEflIVnhH8">
									<div class="video-btn video-btn-xl bg-violet-red ico-90">
										<div class="video-block-wrapper"><span class="flaticon-play-button"></span></div>
									</div>
								</a>

								<!-- Preview Image -->
			 					<img class="img-fluid" src="images/dashboard-07.png" alt="video-preview">

							</div>
						</div>
					</div>


				</div>	   <!-- End container -->
			</section> --}}
            <!-- END CONTENT-9 -->




		{{-- 	<!-- FEATURES-4
			============================================= -->
			<section id="features-4" class="wide-60 features-section division">
				<div class="container">


					<!-- FEATURES-4 WRAPPER -->
					<div class="fbox-4-wrapper fbox-4-wide">
						<div class="row row-cols-1 row-cols-md-2">


							<!-- FEATURE BOX #1 -->
		 					<div class="col">
		 						<div class="fbox-4 pr-25 mb-40 wow fadeInUp">

		 							<!-- Icon -->
		 							<div class="fbox-ico">
		 								<div class="fbox-ico-center ico-65">
											<span class="flaticon-dashboard"></span>
										</div>
									</div>

									<!-- Text -->
									<div class="fbox-txt">

										<!-- Title -->
										<h5 class="h5-md">Friendly Interface</h5>

										<!-- Text -->
										<p class="p-lg">Porta semper lacus cursus feugiat primis ultrice ligula risus auctor
											tempus feugiat impedit undo auctor felis augue mauris aoreet tempor
										</p>

									</div>

		 						</div>
		 					</div>


		 					<!-- FEATURE BOX #2 -->
		 					<div class="col">
		 						<div class="fbox-4 pl-25 mb-40 wow fadeInUp">

		 							<!-- Icon -->
		 							<div class="fbox-ico">
		 								<div class="fbox-ico-center ico-65">
											<span class="flaticon-pantone"></span>
										</div>
									</div>

									<!-- Text -->
									<div class="fbox-txt">

										<!-- Title -->
										<h5 class="h5-md">Powerful Options</h5>

										<!-- Text -->
										<p class="p-lg">Porta semper lacus cursus feugiat primis ultrice ligula risus auctor
											tempus feugiat impedit undo auctor felis augue mauris aoreet tempor
										</p>

									</div>

		 						</div>
		 					</div>


		 					<!-- FEATURE BOX #3 -->
		 					<div class="col">
		 						<div class="fbox-4 pr-25 mb-40 wow fadeInUp">

		 							<!-- Icon -->
		 							<div class="fbox-ico">
		 								<div class="fbox-ico-center ico-65">
											<span class="flaticon-folder-3"></span>
										</div>
									</div>

									<!-- Text -->
									<div class="fbox-txt">

										<!-- Title -->
										<h5 class="h5-md">File Manager</h5>

										<!-- Text -->
										<p class="p-lg">Porta semper lacus cursus feugiat primis ultrice ligula risus auctor
											tempus feugiat impedit undo auctor felis augue mauris aoreet tempor
										</p>

									</div>

		 						</div>
		 					</div>


		 					<!-- FEATURE BOX #4 -->
		 					<div class="col">
		 						<div class="fbox-4 pl-25 mb-40 wow fadeInUp">

		 							<!-- Icon -->
		 							<div class="fbox-ico">
		 								<div class="fbox-ico-center ico-65">
											<span class="flaticon-resize"></span>
										</div>
									</div>

									<!-- Text -->
									<div class="fbox-txt">

										<!-- Title -->
										<h5 class="h5-md">Convert Media Files</h5>

										<!-- Text -->
										<p class="p-lg">Porta semper lacus cursus feugiat primis ultrice ligula risus auctor
											tempus feugiat impedit undo auctor felis augue mauris aoreet tempor
										</p>

									</div>

		 						</div>
		 					</div>


		 					<!-- FEATURE BOX #5 -->
		 					<div class="col">
		 						<div class="fbox-4 pr-25 mb-40 wow fadeInUp">

		 							<!-- Icon -->
		 							<div class="fbox-ico">
		 								<div class="fbox-ico-center ico-65">
											<span class="flaticon-share"></span>
										</div>
									</div>

									<!-- Text -->
									<div class="fbox-txt">

										<!-- Title -->
										<h5 class="h5-md">Files Sharing</h5>

										<!-- Text -->
										<p class="p-lg">Porta semper lacus cursus feugiat primis ultrice ligula risus auctor
											tempus feugiat impedit undo auctor felis augue mauris aoreet tempor
										</p>

									</div>

		 						</div>
		 					</div>


		 					<!-- FEATURE BOX #6 -->
		 					<div class="col">
		 						<div class="fbox-4 pl-25 mb-40 wow fadeInUp">

		 							<!-- Icon -->
		 							<div class="fbox-ico">
		 								<div class="fbox-ico-center ico-65">
											<span class="flaticon-layers"></span>
										</div>
									</div>

									<!-- Text -->
									<div class="fbox-txt">

										<!-- Title -->
										<h5 class="h5-md">Extensions & Addons</h5>

										<!-- Text -->
										<p class="p-lg">Porta semper lacus cursus feugiat primis ultrice ligula risus auctor
											tempus feugiat impedit undo auctor felis augue mauris aoreet tempor
										</p>

									</div>

		 						</div>
		 					</div>


		 				</div>
					</div>    <!-- END FEATURES-4 WRAPPER -->


				</div>     <!-- End container -->
			</section>	<!-- END FEATURES-4 --> --}}




			{{-- <!-- CONTENT-5
			============================================= -->
			<section id="content-5" class="content-5 ws-wrapper content-section division">
				<div class="container">
					<div class="content-5-wrapper bg-whitesmoke">
						<div class="row d-flex align-items-center">


							<!-- TEXT BLOCK -->
							<div class="col-md-7 col-lg-6">
								<div class="txt-block left-column wow fadeInRight">

									<!-- Section ID -->
						 			<span class="section-id txt-upcase">Fast Performance</span>

						 			<!-- Title -->
									<h2 class="h2-xs">More productivity with less effort</h2>

									<!-- List -->
									<ul class="simple-list">

										<li class="list-item">
											<p class="p-lg">Fringilla risus, luctus mauris auctor euismod an iaculis luctus
											   magna purus pretium ligula purus and quaerat sapien rutrum mauris auctor
											</p>
										</li>

										<li class="list-item">
											<p class="p-lg">Quaerat sodales sapien euismod purus blandit</p>
										</li>

										<li class="list-item">
											<p class="p-lg">Nemo ipsam egestas volute turpis dolores ligula and aliquam quaerat
											   at sodales sapien purus
											</p>
										</li>

									</ul>

								</div>
							</div>	<!-- END TEXT BLOCK -->


							<!-- IMAGE BLOCK -->
							<div class="col-md-5 col-lg-6">
								<div class="img-block right-column wow fadeInLeft">
									<img class="img-fluid" src="{{static_asset('landing_page/assets/images/img-10.png')}}" alt="content-image">
								</div>
							</div>


						</div>
					</div>    <!-- End row -->
				</div>	   <!-- End container -->
			</section>	<!-- END CONTENT-5 --> --}}




			<!-- CONTENT-3
			============================================= -->
			{{-- <section id="content-3" class="content-3 wide-60 content-section division">
				<div class="container">


					<!-- SECTION TITLE -->
					<div class="row justify-content-center">
						<div class="col-lg-10 col-xl-8">
							<div class="section-title title-01 mb-70">

								<!-- Title -->
								<h2 class="h2-md">Beautiful Creative Solutions</h2>

								<!-- Text -->
								<p class="p-xl">Aliquam a augue suscipit, luctus neque purus ipsum neque dolor primis a libero
								   tempus, blandit and cursus varius and magnis sapien
								</p>

							</div>
						</div>
					</div>


					<!-- TOP ROW -->
					<div class="top-row pb-50">
						<div class="row d-flex align-items-center">


							<!-- IMAGE BLOCK -->
							<div class="col-md-5 col-lg-6">
								<div class="img-block left-column wow fadeInRight">
									<img class="img-fluid" src="images/img-06.png" alt="content-image">
								</div>
							</div>


							<!-- TEXT BLOCK -->
							<div class="col-md-7 col-lg-6">
								<div class="txt-block right-column wow fadeInLeft">

									<!-- Section ID -->
					 				<span class="section-id txt-upcase">Easy Integration</span>

									<!-- Title -->
									<h2 class="h2-xs">Intuitive features, powerful results</h2>

									<!-- Text -->
									<p class="p-lg">Quaerat sodales sapien euismod blandit purus a purus ipsum primis in cubilia
									   laoreet augue luctus magna dolor luctus and egestas sapien egestas vitae nemo volute
									</p>

									<!-- Text -->
									<p class="p-lg">Quaerat sodales sapien euismod blandit at vitae ipsum primis undo and cubilia
									   laoreet augue and luctus magna dolor luctus at egestas sapien vitae nemo egestas volute and
									   turpis dolores aliquam quaerat sodales a sapien
									</p>

								</div>
							</div>	<!-- END TEXT BLOCK -->


						</div>
					</div>	<!-- END TOP ROW -->


					<!-- BOTTOM ROW -->
					<div class="bottom-row">
						<div class="row d-flex align-items-center">


							<!-- TEXT BLOCK -->
							<div class="col-md-7 col-lg-6 order-last order-md-2">
								<div class="txt-block left-column wow fadeInRight">

									<!-- TEXT BOX -->
									<div class="txt-box mb-20">

										<!-- Title -->
										<h5 class="h5-lg">Advanced Performance Made Easy</h5>

										<!-- Text -->
										<p class="p-lg">Quaerat sodales sapien euismod blandit at vitae ipsum primis undo and
										   cubilia laoreet augue and luctus magna dolor luctus at egestas sapien vitae nemo egestas
										   volute and turpis dolores aliquam quaerat sodales a sapien
										</p>

									</div>

									<!-- TEXT BOX -->
									<div class="txt-box">

										<!-- Title -->
										<h5 class="h5-lg">Creative Alternative Solutions</h5>

										<!-- List -->
										<ul class="simple-list">

											<li class="list-item">
												<p class="p-lg">Fringilla risus, luctus mauris auctor euismod an iaculis luctus
												   magna purus pretium ligula purus and quaerat
												</p>
											</li>

											<li class="list-item">
												<p class="p-lg">Nemo ipsam egestas volute turpis dolores undo ultrice aliquam quaerat
												   at sodales sapien purus
												</p>
											</li>

										</ul>

									</div>	<!-- END TEXT BOX -->

								</div>
							</div>	<!-- END TEXT BLOCK -->


							<!-- IMAGE BLOCK -->
							<div class="col-md-5 col-lg-6 order-first order-md-2">
								<div class="img-block right-column wow fadeInLeft">
									<img class="img-fluid" src="images/img-09.png" alt="content-image">
								</div>
							</div>


						</div>
					</div>	<!-- END BOTTOM ROW -->


				</div>	   <!-- End container -->
			</section> --}}
            <!-- END CONTENT-3 -->




			<!-- PROJECTS-2
			============================================= -->
			{{-- <section id="projects-2" class="pb-60 projects-section division">
				<div class="container">


					<!-- SECTION TITLE -->
					<div class="row justify-content-center">
						<div class="col-lg-10 col-xl-8">
							<div class="section-title title-01 mb-70">

								<!-- Title -->
								<h2 class="h2-md">We Care About The Details</h2>

								<!-- Text -->
								<p class="p-xl">Aliquam a augue suscipit, luctus neque purus ipsum neque dolor primis a libero
								   tempus, blandit and cursus varius and magnis sapien
								</p>

							</div>
						</div>
					</div>


					<!-- PROJECTS-2 WRAPPER -->
					<div class="row">
						<div class="col gallery-items-list">
							<div class="masonry-wrap grid-loaded">


								<!-- PROJECT #1 -->
			 					<div class="project-details masonry-image">

		 							<!-- Image -->
					 				<div class="project-preview rel">
						 				<div class="hover-overlay">
											<img class="img-fluid" src="images/projects/project-05.jpg" alt="project-preview" />
											<div class="item-overlay"></div>
										</div>
									</div>

									<!-- Text -->
									<div class="project-txt">

										<!-- Link -->
										<h5 class="h5-md">
											<a href="project-details.html">A ligula risus auctor and justo tempus blandit</a>
										</h5>

										<!-- Text -->
										<p class="p-md grey-color">Graphic Design</p>

									</div>

			 					</div>	<!-- END PROJECT #1 -->


			 					<!-- PROJECT #2 -->
			 					<div class="project-details masonry-image">

		 							<!-- Image -->
					 				<div class="project-preview rel">
						 				<div class="hover-overlay">
											<img class="img-fluid" src="images/projects/project-02.jpg" alt="project-preview" />
											<div class="item-overlay"></div>
										</div>
									</div>

									<!-- Text -->
									<div class="project-txt">

										<!-- Link -->
										<h5 class="h5-md">
											<a href="project-details.html">Integer urna turpis donec and ipsum porta justo</a>
										</h5>

										<!-- Text -->
										<p class="p-md grey-color">UI, Interaction Design</p>

									</div>

			 					</div>	<!-- END PROJECT #2 -->


			 					<!-- PROJECT #3 -->
			 					<div class="project-details masonry-image">

		 							<!-- Image -->
					 				<div class="project-preview rel">
						 				<div class="hover-overlay">
											<img class="img-fluid" src="images/projects/project-04.jpg" alt="project-preview" />
											<div class="item-overlay"></div>
										</div>
									</div>

									<!-- Text -->
									<div class="project-txt">

										<!-- Link -->
										<h5 class="h5-md">
											<a href="project-details.html">Donec sapien augue undo integer turpis cursus</a>
										</h5>

										<!-- Text -->
										<p class="p-md grey-color">UX, Illustration</p>

									</div>

			 					</div>	<!-- END PROJECT #3 -->


			 					<!-- PROJECT #4 -->
			 					<div class="project-details masonry-image">

		 							<!-- Image -->
					 				<div class="project-preview rel">
						 				<div class="hover-overlay">
											<img class="img-fluid" src="images/projects/project-01.jpg" alt="project-preview" />
											<div class="item-overlay"></div>
										</div>
									</div>

									<!-- Text -->
									<div class="project-txt">

										<!-- Link -->
										<h5 class="h5-md">
											<a href="project-details.html">Laoreet undo magna at suscipit undo magna</a>
										</h5>

										<!-- Text -->
										<p class="p-md grey-color">Web Design</p>

									</div>

			 					</div>	<!-- END PROJECT #4 -->


			 					<!-- PROJECT #5 -->
			 					<div class="project-details masonry-image">

		 							<!-- Image -->
					 				<div class="project-preview rel">
						 				<div class="hover-overlay">
											<img class="img-fluid" src="images/projects/project-03.jpg" alt="project-preview" />
											<div class="item-overlay"></div>
										</div>
									</div>

									<!-- Text -->
									<div class="project-txt">

										<!-- Link -->
										<h5 class="h5-md">
											<a href="project-details.html">Donec sapien an augue integer turpis cursus</a>
										</h5>

										<!-- Text -->
										<p class="p-md grey-color">Web Design</p>

									</div>

			 					</div>	<!-- END PROJECT #5 -->


			 					<!-- PROJECT #6 -->
			 					<div class="project-details masonry-image">

		 							<!-- Image -->
					 				<div class="project-preview rel">
						 				<div class="hover-overlay">
											<img class="img-fluid" src="images/projects/project-06.jpg" alt="project-preview" />
											<div class="item-overlay"></div>
										</div>
									</div>

									<!-- Text -->
									<div class="project-txt">

										<!-- Link -->
										<h5 class="h5-md">
											<a href="project-details.html">Donec sapien an augue integer turpis cursus</a>
										</h5>

										<!-- Text -->
										<p class="p-md grey-color">UI, Interaction Design</p>

									</div>

			 					</div>	<!-- END PROJECT #6 -->


		 					</div>
						</div>
					</div>	<!-- END PROJECTS-1 WRAPPER -->


					<!-- MORE PROJECTS -->
			 		<div class="row">
			 			<div class="col">
			 				<div class="more-btn mt-20">
								<a href="projects.html" class="btn btn-violet-red tra-grey-hover">View More Projects</a>
							</div>
						</div>
					</div>	<!-- END DOWNLOAD BUTTON -->


				</div>	   <!-- End container -->
			</section> --}}
            <!-- END PROJECTS-2 -->




			<!-- STATISTIC-1
			============================================= -->
			{{-- <div id="statistic-1" class="bg-01 pt-70 pb-70 statistic-section division">
				<div class="container white-color">


					<!-- STATISTIC-1 WRAPPER -->
					<div class="statistic-1-wrapper">
						<div class="row justify-content-md-center row-cols-1 row-cols-md-3">


							<!-- STATISTIC BLOCK #1 -->
							<div id="sb-1-1" class="col">
								<div class="statistic-block wow fadeInUp">

									<!-- Digit -->
									<h2 class="h2-xl statistic-number"><span class="count-element">28</span>%</h2>

									<!-- Text -->
									<h5 class="h5-lg">Faster Access</h5>

									<!-- Text -->
									<p class="p-lg">Enim nullam tempor at sapien gravida donec a blandit integer posuere porta
									   justo velna
									</p>

								</div>
							</div>


							<!-- STATISTIC BLOCK #2 -->
							<div id="sb-1-2" class="col">
								<div class="statistic-block wow fadeInUp">

									<!-- Digit -->
									<h2 class="h2-xl statistic-number"><span class="count-element">54</span>%</h2>

									<!-- Text -->
									<h5 class="h5-lg">Productivity</h5>

									<!-- Text -->
									<p class="p-lg">Enim nullam tempor at sapien gravida donec a blandit integer posuere porta
									   justo velna
									</p>

								</div>
							</div>


							<!-- STATISTIC BLOCK #3 -->
							<div id="sb-1-3" class="col">
								<div class="statistic-block wow fadeInUp">

									<!-- Digit -->
									<h2 class="h2-xl statistic-number"><span class="count-element">36</span>%</h2>

									<!-- Text -->
									<h5 class="h5-lg">Secure Access</h5>

									<!-- Text -->
									<p class="p-lg">Enim nullam tempor at sapien gravida donec a blandit integer posuere porta
									   justo velna
									</p>

								</div>
							</div>


						</div>
					</div>	<!-- END STATISTIC-1 WRAPPER -->


				</div>	 <!-- End container -->
			</div> --}}
            <!-- END STATISTIC-1 -->





			<!-- TESTIMONIALS-1
			============================================= -->
			{{-- <section id="reviews-1" class="wide-100 reviews-section division">
				<div class="container">


					<!-- SECTION TITLE -->
					<div class="row justify-content-center">
						<div class="col-lg-10 col-xl-8">
							<div class="section-title title-01 mb-70">

								<!-- Title -->
								<h2 class="h2-md">Stories From Our Customers</h2>

								<!-- Text -->
								<p class="p-xl">Aliquam a augue suscipit, luctus neque purus ipsum neque dolor primis a libero
								   tempus, blandit and cursus varius and magnis sapien
								</p>

							</div>
						</div>
					</div>


					<!-- TESTIMONIALS CONTENT -->
					<div class="row">
						<div class="col">
							<div class="owl-carousel owl-theme reviews-1-wrapper">


								<!-- TESTIMONIAL #1 -->
								<div class="review-1">

									<!-- Quote Icon -->
		 							<div class="review-1-ico ico-25">
										<span class="flaticon-left-quote"></span>
									</div>

									<!-- Text -->
									<div class="review-1-txt">

										<!-- Text -->
										<p class="p-lg">Etiam sapien sagittis congue augue massa varius egestas ultrice
										   varius magna a tempus aliquet undo cursus suscipit
										</p>

										<!-- Testimonial Author -->
										<div class="author-data clearfix">

											<!-- Testimonial Avatar -->
											<div class="review-avatar">
												<img src="images/review-author-1.jpg" alt="review-avatar">
											</div>

											<!-- Testimonial Author -->
											<div class="review-author">

												<h6 class="h6-xl">Scott Boxer</h6>
												<p class="p-md">@scott_boxer</p>

												<!-- Rating -->
												<div class="review-rating ico-15 yellow-color">
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-half-empty"></span>
												</div>

											</div>

										</div>	<!-- End Testimonial Author -->

									</div>	<!-- End Text -->

								</div>	<!-- END TESTIMONIAL #1 -->


								<!-- TESTIMONIAL #2 -->
								<div class="review-1">

									<!-- Quote Icon -->
		 							<div class="review-1-ico ico-25">
										<span class="flaticon-left-quote"></span>
									</div>

									<!-- Text -->
									<div class="review-1-txt">

										<!-- Text -->
										<p class="p-lg">At sagittis congue augue and egestas magna ipsum vitae a purus ipsum
										   primis in cubilia laoreet augue egestas luctus and donec diam ultrice ligula magna
										   suscipit lectus gestas augue into cubilia
										</p>

										<!-- Testimonial Author -->
										<div class="author-data clearfix">

											<!-- Testimonial Avatar -->
											<div class="review-avatar">
												<img src="images/review-author-2.jpg" alt="review-avatar">
											</div>

											<!-- Testimonial Author -->
											<div class="review-author">

												<h6 class="h6-xl">Joel Peterson</h6>
												<p class="p-md">Internet Surfer</p>

												<!-- Rating -->
												<div class="review-rating ico-15 yellow-color">
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-half-empty"></span>
												</div>

											</div>

										</div>	<!-- End Testimonial Author -->

									</div>	<!-- End Text -->

								</div>	<!-- END TESTIMONIAL #2 -->


								<!-- TESTIMONIAL #3 -->
								<div class="review-1">

									<!-- Quote Icon -->
		 							<div class="review-1-ico ico-25">
										<span class="flaticon-left-quote"></span>
									</div>

									<!-- Text -->
									<div class="review-1-txt">

										<!-- Text -->
										<p class="p-lg">Mauris donec magnis sapien etiam sapien congue augue egestas et ultrice
										   vitae purus diam integer a congue magna ligula undo egestas magna at suscipit feugiat
										   primis
										</p>

										<!-- Testimonial Author -->
										<div class="author-data clearfix">

											<!-- Testimonial Avatar -->
											<div class="review-avatar">
												<img src="images/review-author-3.jpg" alt="review-avatar">
											</div>

											<!-- Testimonial Author -->
											<div class="review-author">

												<h6 class="h6-xl">Marisol19</h6>
												<p class="p-md">@marisol19</p>

												<!-- Rating -->
												<div class="review-rating ico-15 yellow-color">
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-half-empty"></span>
												</div>

											</div>

										</div>	<!-- End Testimonial Author -->

									</div>	<!-- End Text -->

								</div>	<!-- END TESTIMONIAL #3 -->


								<!-- TESTIMONIAL #4 -->
								<div class="review-1">

									<!-- Quote Icon -->
		 							<div class="review-1-ico ico-25">
										<span class="flaticon-left-quote"></span>
									</div>

									<!-- Text -->
									<div class="review-1-txt">

										<!-- Text -->
										<p class="p-lg">Mauris donec a magnis sapien etiam sapien congue augue pretium ligula
										   lectus aenean a magna undo mauris lectus laoreet tempor egestas
										</p>

										<!-- Testimonial Author -->
										<div class="author-data clearfix">

											<!-- Testimonial Avatar -->
											<div class="review-avatar">
												<img src="images/review-author-4.jpg" alt="review-avatar">
											</div>

											<!-- Testimonial Author -->
											<div class="review-author">

												<h6 class="h6-xl">Leslie D.</h6>
												<p class="p-md">Web Developer</p>

												<!-- Rating -->
												<div class="review-rating ico-15 yellow-color">
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-half-empty"></span>
												</div>

											</div>

										</div>	<!-- End Testimonial Author -->

									</div>	<!-- End Text -->

								</div>	<!-- END TESTIMONIAL #4 -->


								<!-- TESTIMONIAL #5 -->
								<div class="review-1">

									<!-- Quote Icon -->
		 							<div class="review-1-ico ico-25">
										<span class="flaticon-left-quote"></span>
									</div>

									<!-- Text -->
									<div class="review-1-txt">

										<!-- Text -->
										<p class="p-lg">An augue cubilia laoreet magna suscipit egestas and ipsum a lectus purus ipsum
										   primis and augue ultrice ligula and egestas a suscipit lectus gestas undo auctor tempus
										   feugiat impedit
										</p>

										<!-- Testimonial Author -->
										<div class="author-data clearfix">

											<!-- Testimonial Avatar -->
											<div class="review-avatar">
												<img src="images/review-author-5.jpg" alt="review-avatar">
											</div>

											<!-- Testimonial Author -->
											<div class="review-author">

												<h6 class="h6-xl">Jennifer Harper</h6>
												<p class="p-md">App Developer</p>

												<!-- Rating -->
												<div class="review-rating ico-15 yellow-color">
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-half-empty"></span>
												</div>

											</div>

										</div>	<!-- End Testimonial Author -->

									</div>	<!-- End Text -->

								</div>	<!-- END TESTIMONIAL #5 -->


								<!-- TESTIMONIAL #6 -->
								<div class="review-1">

									<!-- Quote Icon -->
		 							<div class="review-1-ico ico-25">
										<span class="flaticon-left-quote"></span>
									</div>

									<!-- Text -->
									<div class="review-1-txt">

										<!-- Text -->
										<p class="p-lg">An augue cubilia laoreet undo magna ipsum semper suscipit egestas magna
										   ipsum ligula a vitae purus and ipsum primis cubilia magna suscipit
										</p>

										<!-- Testimonial Author -->
										<div class="author-data clearfix">

											<!-- Testimonial Avatar -->
											<div class="review-avatar">
												<img src="images/review-author-6.jpg" alt="review-avatar">
											</div>

											<!-- Testimonial Author -->
											<div class="review-author">

												<h6 class="h6-xl">Jonathan Barnes</h6>
												<p class="p-md">jQuery Programmer</p>

												<!-- Rating -->
												<div class="review-rating ico-15 yellow-color">
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-half-empty"></span>
												</div>

											</div>

										</div>	<!-- End Testimonial Author -->

									</div>	<!-- End Text -->

								</div>	<!-- END TESTIMONIAL #6 -->


								<!-- TESTIMONIAL #7 -->
								<div class="review-1">

									<!-- Quote Icon -->
		 							<div class="review-1-ico ico-25">
										<span class="flaticon-left-quote"></span>
									</div>

									<!-- Text -->
									<div class="review-1-txt">

										<!-- Text -->
										<p class="p-lg">Augue egestas diam tempus volutpat egestas augue in cubilia laoreet magna
										   suscipit luctus dolor and blandit vitae purus diam tempus an aliquet porta rutrum gestas
										</p>

										<!-- Testimonial Author -->
										<div class="author-data clearfix">

											<!-- Testimonial Avatar -->
											<div class="review-avatar">
												<img src="images/review-author-7.jpg" alt="review-avatar">
											</div>

											<!-- Testimonial Author -->
											<div class="review-author">

												<h6 class="h6-xl">Mike Harris</h6>
												<p class="p-md">Graphic Designer</p>

												<!-- Rating -->
												<div class="review-rating ico-15 yellow-color">
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-half-empty"></span>
												</div>

											</div>

										</div>	<!-- End Testimonial Author -->

									</div>	<!-- End Text -->

								</div>	<!-- END TESTIMONIAL #7 -->


								<!-- TESTIMONIAL #8 -->
								<div class="review-1">

									<!-- Quote Icon -->
		 							<div class="review-1-ico ico-25">
										<span class="flaticon-left-quote"></span>
									</div>

									<!-- Text -->
									<div class="review-1-txt">

										<!-- Text -->
										<p class="p-lg">Augue at vitae purus tempus egestas volutpat augue undo cubilia laoreet
										   magna suscipit luctus dolor blandit at purus tempus feugiat impedit
										</p>

										<!-- Testimonial Author -->
										<div class="author-data clearfix">

											<!-- Testimonial Avatar -->
											<div class="review-avatar">
												<img src="images/review-author-8.jpg" alt="review-avatar">
											</div>

											<!-- Testimonial Author -->
											<div class="review-author">

												<h6 class="h6-xl">Evelyn Martinez</h6>
												<p class="p-md">WordPress Consultant</p>

												<!-- Rating -->
												<div class="review-rating ico-15 yellow-color">
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-1"></span>
													<span class="flaticon-star-half-empty"></span>
												</div>

											</div>

										</div>	<!-- End Testimonial Author -->

									</div>	<!-- End Text -->

								</div>	<!-- END TESTIMONIAL #8 -->


							</div>
						</div>
					</div>	<!-- END TESTIMONIALS CONTENT -->


				</div>     <!-- End container -->
			</section> --}}
            <!-- END TESTIMONIALS-1 -->




			<!-- BRANDS-2
			============================================= -->
			<div id="brands-2" class="pt-70 pb-70 brands-section division">
				<div class="container">

					<!-- SECTION TITLE -->
					<div class="row justify-content-center">
						<div class="col-lg-10 col-xl-8">
							<div class="section-title title-01 mb-70">

								<!-- Title -->
								<h2 class="h2-md">Rejoignez ces marques</h2>

								<!-- Text -->
								<p class="p-xl">qui font confiance à KURUBA pour la distribution de leurs produits
								</p>

							</div>
						</div>
					</div>

					<!-- BRANDS-2 WRAPPER -->
					<div class="brands-2-wrapper">
						<div class="row justify-content-md-center row-cols-2 row-cols-sm-3 row-cols-md-5">
                            @php $top10_brands = json_decode(get_setting('top10_brands')); @endphp
                                @foreach ($top10_brands as $key => $value)
                                    @php $brand = \App\Models\Brand::find($value); @endphp
                                    @if ($brand != null)
                                        <!-- BRAND LOGO IMAGE -->
                                        <div class="col">
                                            <div class="brand-logo">
                                                <a href="#">
                                                    <img class="img-fluid" src="{{ uploaded_asset($brand->logo) }}" alt="{{ $brand->getTranslation('name') }}" onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"/>
                                                </a>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach

						</div>
					</div>  <!-- END BRANDS-2 WRAPPER -->


				</div>     <!-- End container -->
			</div>	<!-- END BRANDS-2 -->

			<!-- FAQs-2
			============================================= -->
			{{-- <section id="faqs-2" class="wide-60 faqs-section division">
				<div class="container">


					<!-- SECTION TITLE -->
					<div class="row justify-content-center">
						<div class="col-md-10 col-lg-8">
							<div class="section-title title-02 mb-75">

								<!-- Section ID -->
					 			<span class="section-id txt-upcase">Frequently Asked Questions</span>

								<!-- Title -->
								<h2 class="h2-xs">Everything you need to know before getting started</h2>

							</div>
						</div>
					</div>


					<!-- FAQs-2 QUESTIONS -->
					<div class="faqs-2-questions">
						<div class="row row-cols-1 row-cols-lg-2">


							<!-- QUESTIONS HOLDER -->
							<div class="col">
								<div class="questions-holder pr-15">


									<!-- QUESTION #1 -->
									<div class="question wow fadeInUp">

										<!-- Question -->
										<h5 class="h5-md">Can I see OLMO in action before purchasing?</h5>

										<!-- Answer -->
										<p class="p-lg">Etiam amet mauris suscipit in odio integer congue metus vitae arcu mollis
										   blandit ultrice ligula egestas and magna suscipit lectus magna suscipit luctus blandit
										   vitae
										</p>

									</div>


									<!-- QUESTION #2 -->
									<div class="question wow fadeInUp">

										<!-- Question -->
										<h5 class="h5-md">What are the requirements for using OLMO?</h5>

										<!-- Answer -->
										<p class="p-lg">An enim nullam tempor sapien gravida a donec ipsum enim an porta justo
										   integer at velna vitae auctor integer congue undo magna at pretium purus pretium
										</p>

									</div>


									<!-- QUESTION #3 -->
									<div class="question wow fadeInUp">

										<!-- Question -->
										<h5 class="h5-md">Can I use OLMO on different devices?</h5>

										<!-- Answer -->
										<ul class="simple-list">

											<li class="list-item">
												<p class="p-lg">Fringilla risus, luctus mauris orci auctor purus ligula euismod
												   pretium purus pretium rutrum tempor sapien
												</p>
											</li>

											<li class="list-item">
												<p class="p-lg">Nemo ipsam egestas volute turpis dolores ut aliquam quaerat sodales
												   sapien undo pretium a purus
												</p>
											</li>

										</ul>

									</div>


								</div>
							</div>	<!-- END QUESTIONS HOLDER -->


							<!-- QUESTIONS HOLDER -->
							<div class="col">
								<div class="questions-holder pl-15">


									<!-- QUESTION #4 -->
									<div class="question wow fadeInUp">

										<!-- Question -->
										<h5 class="h5-md">Do you have a free trial?</h5>

										<!-- Answer -->
										<p class="p-lg">Cubilia laoreet augue egestas and luctus donec curabite diam vitae dapibus
										   libero and quisque gravida donec and neque. Blandit justo aliquam molestie nunc sapien
										</p>

									</div>


									<!-- QUESTION #5 -->
									<div class="question wow fadeInUp">

										<!-- Question -->
										<h5 class="h5-md">How does OLMO handle my privacy?</h5>

										<!-- Answer -->
										<p class="p-lg">Etiam amet mauris suscipit sit amet in odio. Integer congue leo metus.
										   Vitae arcu mollis blandit ultrice ligula
										</p>

										<!-- Answer -->
										<p class="p-lg">An enim nullam tempor sapien gravida donec congue leo metus. Vitae arcu
											mollis blandit integer at velna
										</p>

									</div>


									<!-- QUESTION #6 -->
									<div class="question wow fadeInUp">

										<!-- Question -->
										<h5 class="h5-md">I have an issue with my account</h5>

										<!-- Answer -->
										<ul class="simple-list">

											<li class="list-item">
												<p class="p-lg">Fringilla risus, luctus mauris orci auctor purus</p>
											</li>

											<li class="list-item">
												<p class="p-lg">Quaerat sodales sapien euismod blandit purus and ipsum primis in
												   cubilia laoreet augue luctus
												</p>
											</li>

										</ul>

									</div>


								</div>
							</div>	<!-- END QUESTIONS HOLDER -->


						</div>	<!-- End row -->
					</div>	<!-- END FAQs-2 QUESTIONS -->


					<!-- MORE QUESTIONS BUTTON -->
					<div class="row">
						<div class="col">
							<div class="more-questions">
								<h5 class="h5-sm">Have more questions? <a href="contacts.html">Ask your question here</a></h5>
							</div>
						</div>
					</div>


				</div>	   <!-- End container -->
			</section> --}}
            <!-- END FAQs-2 -->




			<!-- CALL TO ACTION-6
			============================================= -->
			<section id="cta-6" class="bg-03 pt-70 pb-70 cta-section division">
				<div class="container">
					<div class="row justify-content-md-center">


						<!-- BANNER TEXT -->
						<div class="col col-lg-8">
							<div class="cta-6-txt white-color text-center">

								<!-- Title -->
								<h2 class="h2-sm">Distribuez vos produits sur KURUBA et boostez vos gains</h2>

								<!-- Buttons Group -->
								<div class="btns-group mb-30">
                                    @guest
									<a href="{{ route('user.login') }}" class="btn btn-md btn-violet-red tra-white-hover mr-15">{{ translate('login') }} </a>
									<a href="{{ url('users/registration') }}" class="btn btn-md btn-tra-white white-hover">{{ translate('register') }}</a>
                                    @else  
                                    <a href="{{ route('marketplace') }}" class="btn btn-md btn-violet-red tra-white-hover mr-15">{{ translate('Go to marketplace') }} </a>
                                    @endguest
								</div>

								<!-- Advantages List -->
								{{-- <ul class="advantages text-center clearfix">
									<li class="first-li"><p>Free 30 days trial</p></li>
									<li><p>Exclusive Support</p></li>
									<li class="last-li"><p>No Fees</p></li>
								</ul> --}}

							</div>
						</div>	<!-- END BANNER TEXT -->


					</div>    <!-- End row -->
				</div>	   <!-- End container -->
			</section>	<!-- END CALL TO ACTION-6 -->
@endsection
