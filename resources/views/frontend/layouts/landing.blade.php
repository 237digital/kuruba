<!DOCTYPE html>

@if(\App\Models\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1)
<html dir="rtl" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@else
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endif
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="app-url" content="{{ getBaseURL() }}">
        <meta name="file-base-url" content="{{ getFileBaseURL() }}">

        <title>@yield('meta_title', get_setting('website_name').' | '.get_setting('site_motto'))</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="index, follow">
        <meta name="description" content="@yield('meta_description', get_setting('meta_description') )" />
        <meta name="keywords" content="@yield('meta_keywords', get_setting('meta_keywords') )">

        @yield('meta')

        @if(!isset($detailedProduct) && !isset($customer_product) && !isset($shop) && !isset($page) && !isset($blog))
            <!-- Schema.org markup for Google+ -->
            <meta itemprop="name" content="{{ get_setting('meta_title') }}">
            <meta itemprop="description" content="{{ get_setting('meta_description') }}">
            <meta itemprop="image" content="{{ uploaded_asset(get_setting('meta_image')) }}">

            <!-- Twitter Card data -->
            <meta name="twitter:card" content="product">
            <meta name="twitter:site" content="@publisher_handle">
            <meta name="twitter:title" content="{{ get_setting('meta_title') }}">
            <meta name="twitter:description" content="{{ get_setting('meta_description') }}">
            <meta name="twitter:creator" content="@author_handle">
            <meta name="twitter:image" content="{{ uploaded_asset(get_setting('meta_image')) }}">

            <!-- Open Graph data -->
            <meta property="og:title" content="{{ get_setting('meta_title') }}" />
            <meta property="og:type" content="website" />
            <meta property="og:url" content="{{ route('home') }}" />
            <meta property="og:image" content="{{ uploaded_asset(get_setting('meta_image')) }}" />
            <meta property="og:description" content="{{ get_setting('meta_description') }}" />
            <meta property="og:site_name" content="{{ env('APP_NAME') }}" />
            <meta property="fb:app_id" content="{{ env('FACEBOOK_PIXEL_ID') }}">
        @endif

        <!-- Favicon -->
        <link rel="icon" href="{{ uploaded_asset(get_setting('site_icon')) }}">

        @if (get_setting('google_analytics') == 1)
        <!-- Global site tag (gtag.js) - Google Analytics -->

        <script async src="https://www.googletagmanager.com/gtag/js?id={{ env('TRACKING_ID') }}"></script>

        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{ env('TRACKING_ID') }}');
        </script>
        @endif

        @if (get_setting('facebook_pixel') == 1)
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '{{ env('FACEBOOK_PIXEL_ID') }}');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id={{ env('FACEBOOK_PIXEL_ID') }}&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
        @endif

        @php
        echo get_setting('header_script');
        @endphp

        <!-- FAVICON AND TOUCH ICONS -->
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" sizes="152x152" href="images/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="120x120" href="images/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="76x76" href="images/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
        <link rel="icon" href="images/apple-touch-icon.png" type="image/x-icon">

        <!-- GOOGLE FONTS -->
        <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;700&amp;display=swap" rel="stylesheet">

        @include('frontend.inc.landing_page.css')

        @stack('css')

    </head>

	<body>

		<!-- PRELOADER SPINNER
		============================================= -->
		{{-- <div id="loading" class="violet-red-loading">
			<div id="loading-center">
				<div id="loading-center-absolute">
					<div class="object" id="object_one"></div>
					<div class="object" id="object_two"></div>
					<div class="object" id="object_three"></div>
					<div class="object" id="object_four"></div>
				</div>
			</div>
		</div> --}}

		<!-- PAGE CONTENT
		============================================= -->
		<div id="page" class="page @if(\App\Models\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1) rtl-direction @endif">

			@include('frontend.inc.landing_page.header')

            @yield('content')

			@include('frontend.inc.landing_page.footer')

        </div>	<!-- END PAGE CONTENT -->
            <!-- SCRIPTS -->
    <script src="{{ static_asset('assets/js/vendors.js') }}"></script>
    <script src="{{ static_asset('assets/js/aiz-core.js') }}"></script>

		@include('frontend.inc.landing_page.scripts')

        <script>
            @foreach (session('flash_notification', collect())->toArray() as $message)
                AIZ.plugins.notify('{{ $message['level'] }}', '{{ $message['message'] }}');
            @endforeach
        </script>

        @stack('scripts')
	</body>
</html>
