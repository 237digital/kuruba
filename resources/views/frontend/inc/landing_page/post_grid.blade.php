<!-- BLOG POST #1 -->
<div class="col">
    <div id="bp-1-1" class="blog-1-post mb-40 wow fadeInUp">

        <!-- BLOG POST IMAGE -->
        <div class="blog-post-img">
            <div class="hover-overlay"> 
                <img class="img-fluid" src="{{ uploaded_asset($post->banner) }}" alt="{{  $post->title }}" />
                <div class="item-overlay"></div>
            </div>
        </div>

        <!-- BLOG POST TEXT -->
        <div class="blog-post-txt">

            <!-- Post Tag -->
            <p class="p-md post-tag">OLMO News &ensp;|&ensp; June 12, 2021</p>	

            <!-- Post Link -->
            <h5 class="h5-md">
                <a href="{{ url("blog").'/'. $post->slug }}">{{ $post->title }} </a>
            </h5>

            <!-- Text -->
            <p class="p-lg">{{ $post->short_description }}</p>

        </div>	<!-- END BLOG POST TEXT -->

    </div>
</div>	
<!-- END  BLOG POST #1 -->