<!-- EXTERNAL SCRIPTS -->
    <script src="{{ static_asset('landing_page/assets/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ static_asset('landing_page/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ static_asset('landing_page/assets/js/modernizr.custom.js') }}"></script>
    <script src="{{ static_asset('landing_page/assets/js/jquery.easing.js') }}"></script>
    <script src="{{ static_asset('landing_page/assets/js/jquery.appear.js') }}"></script>
    <script src="{{ static_asset('landing_page/assets/js/jquery.scrollto.js') }}"></script>
    <script src="{{ static_asset('landing_page/assets/js/menu.js') }}"></script>
    <script src="{{ static_asset('landing_page/assets/js/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ static_asset('landing_page/assets/js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ static_asset('landing_page/assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ static_asset('landing_page/assets/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ static_asset('landing_page/assets/js/quick-form.js') }}"></script>
    <script src="{{ static_asset('landing_page/assets/js/jquery.validate.min.js') }}"></script>
    <script src="{{ static_asset('landing_page/assets/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ static_asset('landing_page/assets/js/wow.js') }}"></script>
    

    <!-- Custom Script -->
    <script src="{{ static_asset('landing_page/assets/js/custom.js') }}"></script> 

    @if (get_setting('facebook_chat') == 1)
    <script type="text/javascript">
        window.fbAsyncInit = function() {
            FB.init({
              xfbml            : true,
              version          : 'v3.3'
            });
          };

          (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <div id="fb-root"></div>
    <!-- Your customer chat code -->
    <div class="fb-customerchat"
      attribution=setup_tool
      page_id="{{ env('FACEBOOK_PAGE_ID') }}">
    </div>
@endif

<script>
     $(document).ready(function() {

            if ($('#lang-change').length > 0) {
                $('#lang-change .sub-menu a').each(function() {
                    $(this).on('click', function(e){
                        e.preventDefault();
                        var $this = $(this);
                        var locale = $this.data('flag');
                        $.post('{{ route('language.change') }}',{_token: AIZ.data.csrf, locale:locale}, function(data){
                            location.reload();
                        });

                    });
                });
            }
        });
</script>
