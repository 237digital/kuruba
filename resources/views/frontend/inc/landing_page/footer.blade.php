@php
    $footer_logo = get_setting('header_logo');
@endphp
<footer id="footer-4" class="footer division">
    <div class="container">
        <!-- FOOTER CONTENT -->
        <div class="row">	


            <!-- FOOTER INFO -->
            <div class="col-lg-4 mb-40">
                <div class="footer-info">
                    <a href="{{ url('/') }}" class="logo-black">
                        @if($footer_logo != null)
                            <img class="footer-logo" src="{{ uploaded_asset($footer_logo) }}" alt="{{ env('APP_NAME') }}">
                        @else
                            <img class="footer-logo" src="{{ static_asset('assets/img/logo.png') }}" alt="{{ env('APP_NAME') }}">
                        @endif
                    </a>
                </div>
                <div class="my-3">
                    {!! get_setting('about_us_description',null,App::getLocale()) !!}
                </div>
                	
            </div>	


            <!-- FOOTER LINKS -->
            <div class="col-sm-12 col-md-4 col-lg-4 col-lg-4 col-xl-4">
                <div class="footer-links mb-40">
                
                    <!-- Title -->
                    <h6 class="h6-xl">{{ translate('Contact Info') }}</h6>

                    <!-- Footer Links -->
                    <ul class="foo-links text-secondary clearfix">
                        <li>
                            <p><span class="flaticon-home"></span> <span class="opacity-70"> {{ get_setting('contact_address',null,App::getLocale()) }}</span></p>
                           
                        </li>
                        <li>
                            <p><span class="flaticon-phone-call"></span> <span class="opacity-70"> {{ get_setting('contact_phone') }}</span></p>
                            
                        </li>	
                        <li>
                            <p><span class="flaticon-email"></span><span class="opacity-70">  
                                <a href="mailto:{{ get_setting('contact_email') }}" class="text-reset">{{ get_setting('contact_email')  }}</a>
                            </span></p>
                        </li>
                    </ul>
                </div>
            </div>	


            <!-- FOOTER LINKS -->
            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="footer-links mb-20">
                                    
                    <!-- Title -->
                    <h6 class="h6-xl">{{translate("Connect With Us")}}</h6>
                    @if ( get_setting('show_social_links') )
                        <ul class="footer-socials text-secondary ico-25 text-center clearfix">
                            @if ( get_setting('facebook_link') !=  null )
                            <li class="list-inline-item">
                                <a href="{{ get_setting('facebook_link') }}" target="_blank" class="facebook"><span class="flaticon-facebook"></span></a>
                            </li>
                            @endif
                            @if ( get_setting('twitter_link') !=  null )
                            <li class="list-inline-item">
                                <a href="{{ get_setting('twitter_link') }}" target="_blank" class="twitter"><span class="flaticon-twitter"></span></a>
                            </li>
                            @endif
                            @if ( get_setting('instagram_link') !=  null )
                            <li class="list-inline-item">
                                <a href="{{ get_setting('instagram_link') }}" target="_blank" class="instagram"><span class="flaticon-instagram"></span></a>
                            </li>
                            @endif
                           
                            @if ( get_setting('linkedin_link') !=  null )
                            <li class="list-inline-item">
                                <a href="{{ get_setting('linkedin_link') }}" target="_blank" class="linkedin"><span class="flaticon-linkedin"></span></a>
                            </li>
                            @endif
                        </ul>
                    @endif
                </div>	
                <div class="input-group mb-30">
                    <form class="form-inline" method="POST" action="{{ route('subscribers.store') }}">
                        @csrf
                        <input type="email" name="email" class="form-control email" placeholder="{{ translate('Your Email Address') }}" autocomplete="off" required>
                        <span class="input-group-btn form-btn">
                            <button type="submit" class="btn btn-sm btn-orange black-hover submit" style="height: 38px; padding:1px 30px;font-size:14px;">{{ translate('Subscribe') }}</button>
                        </span>	
                    </form>									
                </div>
             
                <div class="w-300px mw-100 mx-auto mx-md-0">
                    @if(get_setting('play_store_link') != null)
                        <a href="{{ get_setting('play_store_link') }}" target="_blank" class="d-inline-block mr-3 ml-0">
                            <img src="{{ static_asset('assets/img/play.png') }}" class="mx-100 h-40px" style="height: 40px;">
                        </a>
                    @endif
                    @if(get_setting('app_store_link') != null)
                        <a href="{{ get_setting('app_store_link') }}" target="_blank" class="d-inline-block pull-left">
                            <img src="{{ static_asset('assets/img/app.png') }}" class="mx-100 h-40px" style="height: 40px;">
                        </a>
                    @endif
                </div>
            </div>	


        </div>	<!-- END FOOTER CONTENT -->

        <hr>

        <!-- BOTTOM FOOTER -->
        <div class="bottom-footer">
            <div class="row row-cols-1 row-cols-md-2 d-flex align-items-center">


                <!-- FOOTER COPYRIGHT -->
                <div class="col">
                    <div class="footer-copyright" current-verison="{{get_setting("current_version")}}">&copy;
                        {!! get_setting('frontend_copyright_text',null,App::getLocale()) !!}
                    </div>
                </div>


                <!-- BOTTOM FOOTER LINKS -->
                {{-- <div class="col">
                    <ul class="bottom-footer-list text-secondary text-end">
                        <li class="first-li"><p><a href="#">Privacy Policy</a></p></li>
                        <li class="last-li"><p><a href="#">Terms &amp; Conditions</a></p></li>
                    </ul>
                </div> --}}


            </div>  <!-- End row -->
        </div>	<!-- BOTTOM FOOTER -->
    </div>
</footer>