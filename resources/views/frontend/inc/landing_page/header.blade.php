<!-- HEADER -->
<header id="header" class="header tra-menu navbar-dark">
    <div class="header-wrapper">
        <!-- HEADER LOGO -->
        @php
            $header_logo = get_setting('header_logo');
        @endphp

        <!-- MOBILE HEADER -->
        <div class="wsmobileheader clearfix">
            <span class="smllogo">
                @if($header_logo != null)
                    <img src="{{ uploaded_asset($header_logo) }}" alt="{{ env('APP_NAME') }}">
                @else
                    <img src="{{ static_asset('assets/img/logo.png') }}" alt="{{ env('APP_NAME') }}">
                @endif
            </span>
            <a id="wsnavtoggle" class="wsanimated-arrow">
                <span></span>
            </a>
         </div>

         <!-- NAVIGATION MENU -->
          <div class="wsmainfull menu clearfix">
            <div class="wsmainwp clearfix">

                <div class="desktoplogo">
                    <a href="{{ url('/') }}" class="logo-black">
                        @if($header_logo != null)
                            <img src="{{ uploaded_asset($header_logo) }}" alt="{{ env('APP_NAME') }}" style="max-height: 70px;">
                        @else
                            <img src="{{ static_asset('assets/img/logo.png') }}" alt="{{ env('APP_NAME') }}" style="max-height: 70px;">
                        @endif
                    </a>
                </div>
                <div class="desktoplogo">
                    <a href="{{ url('/') }}" class="logo-white">
                        @if(get_setting('footer_logo') != null)
                            <img src="{{ static_asset('assets/img/placeholder-rect.jpg') }}" data-src="{{ uploaded_asset(get_setting('footer_logo')) }}" alt="{{ env('APP_NAME') }}">
                        @else
                            <img src="{{ static_asset('assets/img/placeholder-rect.jpg') }}" data-src="{{ static_asset('assets/img/logo.png') }}" alt="{{ env('APP_NAME') }}" >
                        @endif
                    </a>
                </div>


                <!-- MAIN MENU -->
                <nav class="wsmenu clearfix">
                    <ul class="wsmenu-list nav-violet-red-hover">

                        
                        <!-- SIMPLE NAVIGATION LINK -->
                        <li class="nl-simple" aria-haspopup="true"><a href="{{url('/')}}/#how-it-works">{{ translate('How it works') }} </a></li>

                        <!-- SIMPLE NAVIGATION LINK -->
                        <li class="nl-simple" aria-haspopup="true"><a href="{{url('/')}}/#about">{{ translate('About us') }} </a></li>

                        <!-- DROPDOWN MENU -->
                        {{-- <li aria-haspopup="true"><a href="#">About  --}}
                            @if(get_setting('show_language_switcher') == 'on')
                            <li aria-haspopup="true" id="lang-change">
                                @php
                                    if(Session::has('locale')){
                                        $locale = Session::get('locale', Config::get('app.locale'));
                                    }
                                    else{
                                        $locale = 'en';
                                    }
                                @endphp
                                <a href="javascript:void(0)" data-toggle="dropdown" data-display="static">
                                    <img src="{{ static_asset('assets/img/flags/'.$locale.'.png') }}" data-src="{{ static_asset('assets/img/flags/'.$locale.'.png') }}" class="mr-2 lazyload" alt="{{ \App\Models\Language::where('code', $locale)->first()->name }}" height="11">
                                    <span class="opacity-60">{{ ucfirst(\App\Models\Language::where('code', $locale)->first()->code) }}</span><span class="wsarrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    @foreach (\App\Models\Language::all() as $key => $language)
                                        <li>
                                            <a href="javascript:void(0)" data-flag="{{ $language->code }}" class=" @if($locale == $language) active @endif">
                                                <img src="{{ static_asset('assets/img/flags/'.$language->code.'.png') }}" data-src="{{ static_asset('assets/img/flags/'.$language->code.'.png') }}" alt="{{ $language->name }}" height="11">
                                                <span class="language">{{ $language->code }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endif
                        </li>
                        @guest
                        <!-- HEADER BUTTON -->
                        <li class="nl-simple" aria-haspopup="true">
                            <a href="{{ route('user.login') }}" class="btn btn-orange tra-orange-hover last-link white-color">{{ translate('login') }} </a>
                        </li>
                        @else
                        <!-- HEADER BUTTON -->
                        <li class="nl-simple" aria-haspopup="true">
                            <a href="{{ route('marketplace') }}" class="btn btn-orange tra-orange-hover last-link white-color">{{ translate('Go to marketplace') }} </a>
                        </li>
                        <li class="nl-simple" aria-haspopup="true">
                            <a href="{{ route('logout') }}">
                                <span class="flaticon-outside"></span>
                            </a>
                        </li>
                        @endguest






                        <!-- HEADER SOCIAL LINKS
                        <li class="nl-simple white-color header-socials ico-20 clearfix" aria-haspopup="true">
                            <span><a href="#" class="ico-facebook"><span class="flaticon-facebook"></span></a></span>
                            <span><a href="#" class="ico-twitter"><span class="flaticon-twitter"></span></a></span>
                            <span><a href="#" class="ico-instagram"><span class="flaticon-instagram"></span></a></span>
                            <span><a href="#" class="ico-dribbble"><span class="flaticon-dribbble"></span></a></span>
                        </li> -->


                    </ul>
                </nav>	<!-- END MAIN MENU -->


            </div>
        </div>	<!-- END NAVIGATION MENU -->


    </div>     <!-- End header-wrapper -->
</header>
