<link href="{{ static_asset('assets/css/vendors.css') }}" rel="stylesheet">
<!-- BOOTSTRAP CSS -->
<link href="{{ static_asset('landing_page/assets/css/bootstrap.min.css') }}" rel="stylesheet">

<!-- FONT ICONS -->
<link href="{{ static_asset('landing_page/assets/css/bootstrap.min.css') }}" rel="stylesheet">

<!-- PLUGINS STYLESHEET -->
<link href="{{ static_asset('landing_page/assets/css/menu.css') }}" rel="stylesheet">
<link id="effect" href="{{ static_asset('landing_page/assets/css/dropdown-effects/fade-down.css') }}" media="all" rel="stylesheet">
<link href="{{ static_asset('landing_page/assets/css/magnific-popup.css') }}" rel="stylesheet">
<link href="{{ static_asset('landing_page/assets/css/owl.carousel.min.css') }}" rel="stylesheet">
<link href="{{ static_asset('landing_page/assets/css/owl.theme.default.min.css') }}" rel="stylesheet">

<!-- ON SCROLL ANIMATION -->
<link href="{{ static_asset('landing_page/assets/css/animate.css') }}" rel="stylesheet">

<!-- TEMPLATE CSS -->
<link href="{{ static_asset('landing_page/assets/css/style.css') }}" rel="stylesheet">

<!-- FONT ICONS -->
<link href="{{ static_asset('landing_page/assets/css/flaticon.css') }}" rel="stylesheet">

<!-- RESPONSIVE CSS -->
<link href="{{ static_asset('landing_page/assets/css/responsive.css') }}" rel="stylesheet">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">

<script src="{{ static_asset('landing_page/assets/js/particles.min.js') }}"></script>


<script>
    var AIZ = AIZ || {};
    AIZ.local = {
        nothing_selected: '{{ translate('Nothing selected') }}',
        nothing_found: '{{ translate('Nothing found') }}',
        choose_file: '{{ translate('Choose file') }}',
        file_selected: '{{ translate('File selected') }}',
        files_selected: '{{ translate('Files selected') }}',
        add_more_files: '{{ translate('Add more files') }}',
        adding_more_files: '{{ translate('Adding more files') }}',
        drop_files_here_paste_or: '{{ translate('Drop files here, paste or') }}',
        browse: '{{ translate('Browse') }}',
        upload_complete: '{{ translate('Upload complete') }}',
        upload_paused: '{{ translate('Upload paused') }}',
        resume_upload: '{{ translate('Resume upload') }}',
        pause_upload: '{{ translate('Pause upload') }}',
        retry_upload: '{{ translate('Retry upload') }}',
        cancel_upload: '{{ translate('Cancel upload') }}',
        uploading: '{{ translate('Uploading') }}',
        processing: '{{ translate('Processing') }}',
        complete: '{{ translate('Complete') }}',
        file: '{{ translate('File') }}',
        files: '{{ translate('Files') }}',
    }
</script>
