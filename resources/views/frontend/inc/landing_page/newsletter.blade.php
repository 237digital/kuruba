<!-- NEWSLETTER-1 -->
<section id="newsletter-1" class="bg-snow newsletter-section division">
    <div class="container">
        <div class="newsletter-wrapper bg-white">
            <div class="row d-flex align-items-center row-cols-1 row-cols-lg-2">

                <!-- NEWSLETTER TEXT -->	
                <div class="col">
                    <div class="newsletter-txt pr-20">	
                        <h4 class="h4-xl">Stay up to date with our news, ideas and updates</h4>	
                    </div>								
                </div>

                <!-- NEWSLETTER FORM -->
                <div class="col">
                    <form method="POST" action="{{ route('subscribers.store') }}">
                        @csrf
                        <div class="input-group">
                            <input type="email" name="email" autocomplete="off" class="form-control" placeholder="{{ translate('Your Email Address') }}" required id="s-email">								
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-md btn-skyblue tra-grey-hover">{{ translate('Subscribe') }}</button>
                            </span>										
                        </div>

                        <!-- Newsletter Form Notification -->	
                        <label for="s-email" class="form-notification"></label>
                                    
                    </form>							
                </div>	  <!-- END NEWSLETTER FORM -->
            </div>	  <!-- End row -->
        </div>    <!-- End newsletter-wrapper -->
    </div>	   <!-- End container -->	
</section>	
<!-- END NEWSLETTER-1 -->