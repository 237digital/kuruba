@extends('frontend.layouts.landing')

@section('meta_title'){{ $blog->meta_title }}@stop

@section('meta_description'){{ $blog->meta_description }}@stop

@section('meta_keywords'){{ $blog->meta_keywords }}@stop

@section('meta')
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ $blog->meta_title }}">
    <meta itemprop="description" content="{{ $blog->meta_description }}">
    <meta itemprop="image" content="{{ uploaded_asset($blog->meta_img) }}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="{{ $blog->meta_title }}">
    <meta name="twitter:description" content="{{ $blog->meta_description }}">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image" content="{{ uploaded_asset($blog->meta_img) }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ $blog->meta_title }}" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ route('blog.details', $blog->slug) }}" />
    <meta property="og:image" content="{{ uploaded_asset($blog->meta_img) }}" />
    <meta property="og:description" content="{{ $blog->meta_description }}" />
    <meta property="og:site_name" content="{{ env('APP_NAME') }}" />
@endsection

@section('content')
    <!-- SINGLE POST -->
    <section id="single-post" class="wide-100 inner-page-hero single-post-section division">
        <div class="container">
            <!-- SINGLE POST CONTENT -->
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="single-post-wrapper">
                        <!-- SINGLE POST TITLE -->
                        <div class="single-post-title">

                            <!-- CATEGORY -->
                            <p class="p-sm post-tag txt-500 txt-upcase">{{ $blog->category->category_name }}</p>

                            <!-- TITLE -->
                            <h2 class="h2-md">{{ $blog->title }}</h2>

                        </div>	<!-- END SINGLE POST TITLE -->

                        <!-- BLOG POST INNER IMAGE -->
                        <div class="post-inner-img">
                            <img class="img-fluid" src="{{ uploaded_asset($blog->banner) }}" alt="blog-post-image" />		
                        </div>

                        <!-- BLOG POST TEXT -->
                        <div class="single-post-txt">
                            {!! $blog->description !!}
                        </div>	<!-- END BLOG POST TEXT -->

                        @if (get_setting('facebook_comment') == 1)
                        <div>
                            <div class="fb-comments" data-href="{{ route("blog",$blog->slug) }}" data-width="" data-numposts="5"></div>
                        </div>
                        @endif

                        {{-- <!-- SINGLE POST SHARE LINKS -->
                        <div class="row post-share-links d-flex align-items-center">

                            <!-- POST TAGS -->
                            <div class="col-md-9 col-xl-8 post-tags-list">
                                <span><a href="#">Life</a></span>	
                                <span><a href="#">Ideas</a></span>
                                <span><a href="#">Story</a></span>	
                                <span><a href="#">Web Design</a></span>										
                            </div>

                            <!-- POST SHARE ICONS -->
                            <div class="col-md-3 col-xl-4 post-share-list text-end">
                                <ul class="share-social-icons ico-25 text-center clearfix">								
                                    <li><a href="#" class="share-ico"><span class="flaticon-twitter"></span></a></li>
                                    <li><a href="#" class="share-ico"><span class="flaticon-facebook"></span></a></li>
                                    <li><a href="#" class="share-ico"><span class="flaticon-bookmark"></span></a></li>
                                </ul>
                            </div>

                        </div>	<!-- END SINGLE POST SHARE --> --}}

                    </div>
                </div>
            </div>	<!-- END SINGLE POST CONTENT -->


        </div>     <!-- End container -->
    </section>	
    <!-- END SINGLE POST -->

    @if(isset($others))
        <!-- BLOG-1 -->
        <section id="blog-1" class="bg-whitesmoke-gradient wide-60 blog-section division">				
            <div class="container">

                <!-- SECTION TITLE -->	
                <div class="row justify-content-center">	
                    <div class="col-lg-10 col-xl-8">
                        <div class="section-title title-01 mb-70">		
                            <h2 class="h2-md">{{ translate('Keep Reading') }}...</h2>	
                        </div>	
                    </div>
                </div>


                <!-- BLOG POSTS -->
                <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
                    @foreach($others->take(3) as $post)
                        <!-- BLOG POST #1 -->
                        @include('frontend.inc.landing_page.post_grid')	
                        <!-- END  BLOG POST #1 -->
                    @endforeach
                </div>	<!-- END BLOG POSTS -->
            </div>	   <!-- End container -->		
        </section>	
        <!-- END BLOG-1 -->
    @endif
    
    @include('frontend.inc.landing_page.newsletter')

@endsection


@push('scripts')
    @if (get_setting('facebook_comment') == 1)
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v9.0&appId={{ env('FACEBOOK_APP_ID') }}&autoLogAppEvents=1" nonce="ji6tXwgZ"></script>
    @endif
@endpush