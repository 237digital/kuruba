@extends('frontend.layouts.landing')

@section('content')


			<!-- BLOG POSTS LISTING -->
			<section id="blog-page" class="bg-snow wide-50 inner-page-hero blog-page-section division">
				<div class="container">


					<!-- SECTION TITLE -->	
					<div class="row justify-content-center">	
						<div class="col-md-10 col-lg-8">
							<div class="section-title title-02 mb-85">	
								<h2 class="h2-xs">Relevant news and more for you. Welcome to our blog</h2>
							</div>	
						</div>
					</div>


					<!-- FEATURED POST -->
					<div class="rel blog-post-wide featured-post">
	 					<div class="row d-flex align-items-center">

	 						<!-- Featured Badge -->
	 						<div class="featured-badge ico-25 bg-whitesmoke yellow-color">
	 							<span class="flaticon-star-1"></span>
	 						</div>
																		
							<!-- BLOG POST IMAGE -->
				 			<div class="col-lg-7 blog-post-img">
				 				<div class="hover-overlay"> 
									<img class="img-fluid" src="images/blog/featured-post.jpg" alt="blog-post-image" />	
									<div class="item-overlay"></div>
								</div>
							</div>

							<!-- BLOG POST TEXT -->
							<div class="col-lg-5 blog-post-txt">

								<!-- Post Tag -->
								<p class="p-md post-tag">OLMO News &ensp;|&ensp; May 18, 2021</p>

								<!-- Post Link -->
								<h5 class="h5-xl">
									<a href="single-post.html">Tempor sapien donec gravida a suscipit and porta justo vitae</a>
								</h5>

								<!-- Text -->
								<p class="p-lg">Aliqum mullam blandit vitae tempor sapien and donec lipsum gravida and porta 
								   undo velna dolor in cubilia...
								</p>

								<!-- Post Meta -->
								<div class="post-meta"><p class="p-md">38 Comments</p></div>	

							</div>	<!-- END BLOG POST TEXT -->

						</div>   <!-- End row -->
				 	</div>	<!-- END FEATURED POST -->


				 	<!-- POSTS WRAPPER -->
					<div class="posts-wrapper">


						<!-- BLOG POSTS CATEGORY --> 
						<div class="row">
							<div class="col-md-12">
								<h5 class="h5-lg posts-category">Latest Articles</h5>
							</div>
						</div>


						<!-- BLOG POSTS -->
						<div class="row">	
							<div class="col gallery-items-list">
								<div class="masonry-wrap grid-loaded">
                                    @foreach($blogs as $post)
                                        <!-- BLOG POST #1 -->
                                        <div class="blog-3-post masonry-image">

                                            <!-- BLOG POST IMAGE -->
                                            <div class="blog-post-img">
                                                <div class="hover-overlay"> 
                                                    <img class="img-fluid" src="{{ uploaded_asset($post->banner) }}" alt="{{  $post->title }}" />
                                                    <div class="item-overlay"></div>
                                                </div>
                                            </div>

                                            <!-- BLOG POST TEXT -->
                                            <div class="blog-post-txt">

                                                <!-- Post Tag -->
                                                <p class="p-md post-tag">OLMO News &ensp;|&ensp; JuLy 31, 2021</p>	

                                                <!-- Post Link -->
                                                <h5 class="h5-md">
                                                    <a href="{{ url("blog").'/'. $post->slug }}">
                                                        {{  $post->title }}
                                                    </a>
                                                </h5>

                                            </div>	
                                            <!-- END BLOG POST TEXT -->
                                        </div>	
                                        <!-- END BLOG POST #1 -->
                                    @endforeach
								</div>
							</div>
						</div>	<!-- END BLOG POSTS -->
					</div>	<!-- END POSTS WRAPPER -->
				</div>     <!-- End container -->
			</section>	
            <!-- END BLOG POSTS LISTING -->

			<!-- PAGE PAGINATION -->
			<div class="bg-snow pb-100 page-pagination division">
				<div class="container">
					<div class="row">	
						<div class="col-md-12">
							{{ $blogs->links() }}
						</div>
					</div>  <!-- End row -->	
				</div> <!-- End container -->
			</div>	
            <!-- END PAGE PAGINATION -->

			@include('frontend.inc.landing_page.newsletter')

{{-- <section class="pt-4 mb-4">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <h1 class="fw-600 h4">{{ translate('Blog')}}</h1>
            </div>
            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">
                            {{ translate('Home')}}
                        </a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('blog') }}">
                            "{{ translate('Blog') }}"
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="pb-4">
    <div class="container">
        <div class="card-columns">
            @foreach($blogs as $blog)
                <div class="card mb-3 overflow-hidden shadow-sm">
                    <a href="{{ url("blog").'/'. $blog->slug }}" class="text-reset d-block">
                        <img
                            src="{{ static_asset('assets/img/placeholder-rect.jpg') }}"
                            data-src="{{ uploaded_asset($blog->banner) }}"
                            alt="{{ $blog->title }}"
                            class="img-fluid lazyload "
                        >
                    </a>
                    <div class="p-4">
                        <h2 class="fs-18 fw-600 mb-1">
                            <a href="{{ url("blog").'/'. $blog->slug }}" class="text-reset">
                                {{ $blog->title }}
                            </a>
                        </h2>
                        @if($blog->category != null)
                        <div class="mb-2 opacity-50">
                            <i>{{ $blog->category->category_name }}</i>
                        </div>
                        @endif
                        <p class="opacity-70 mb-4">
                            {{ $blog->short_description }}
                        </p>
                        <a href="{{ url("blog").'/'. $blog->slug }}" class="btn btn-soft-primary">
                            {{ translate('View More') }}
                        </a>
                    </div>
                </div>
            @endforeach
            
        </div>
        <div class="aiz-pagination aiz-pagination-center mt-4">
            {{ $blogs->links() }}
        </div>
    </div>
</section> --}}
@endsection
