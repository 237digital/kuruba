@extends('frontend.layouts.landing')

@section('content')
@php
    $terms =  \App\Models\Page::where('type', 'terms_conditions_page')->first();
@endphp
<!-- RETURN POLICY -->
<section id="terms-page" class="wide-70 inner-page-hero terms-section division">
    <div class="container">
        <!-- CONTENT -->
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <!-- TITLE -->
                <div class="terms-title text-center">
                    <!-- Title -->
                    <h2 class="h2-md">{{ $terms->getTranslation('title') }}</h2>

                    <!-- Text -->
                    <p class="p-xl grey-color"><a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a> / <a class="text-reset" href="{{ route('terms') }}">"{{ translate('Terms & conditions') }}"</a></p>

                </div>
                <!-- TERMS BOX -->
                <div class="terms-box mt-60">
                    @php
                        echo $terms->getTranslation('content');
                    @endphp
                </div>	<!-- END TERMS BOX -->
            </div>	<!-- END TERMS CONTENT -->
        </div>     <!-- End row -->
    </div>	    <!-- End container -->
</section>
<!-- END RETURN POLICY -->
@endsection
